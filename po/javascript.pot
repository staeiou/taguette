# Translations template for Taguette.
# Copyright (C) 2019 Remi Rampin
# This file is distributed under the same license as the Taguette project.
# Remi Rampin <r@remirampin.com>, 2018.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Taguette 0.6\n"
"Report-Msgid-Bugs-To: hi@taguette.org\n"
"POT-Creation-Date: 2019-05-15 19:12-0400\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.6.0\n"

#: taguette/static/js/taguette.js:410
msgid "Couldn't update project metadata!"
msgstr ""

#: taguette/static/js/taguette.js:466 taguette/static/js/taguette.js:689
msgid "edit"
msgstr ""

#: taguette/static/js/taguette.js:475
msgid "There are no documents in this project yet."
msgstr ""

#: taguette/static/js/taguette.js:533 taguette/static/js/taguette.js:539
msgid "Error uploading file!"
msgstr ""

#: taguette/static/js/taguette.js:569
msgid "Document name cannot be empty"
msgstr ""

#: taguette/static/js/taguette.js:586
msgid "Couldn't update document!"
msgstr ""

#: taguette/static/js/taguette.js:595
#, python-format
msgid "Are you sure you want to delete the document '%(doc)s'?"
msgstr ""

#: taguette/static/js/taguette.js:607
msgid "Couldn't delete document!"
msgstr ""

#: taguette/static/js/taguette.js:699
msgid "There are no tags in this project yet."
msgstr ""

#: taguette/static/js/taguette.js:729
msgid "no tags"
msgstr ""

#: taguette/static/js/taguette.js:782
msgid "Invalid tag name"
msgstr ""

#: taguette/static/js/taguette.js:809
msgid "Couldn't create tag!"
msgstr ""

#: taguette/static/js/taguette.js:818
#, python-format
msgid "Are you sure you want to delete the tag '%(tag)s'?"
msgstr ""

#: taguette/static/js/taguette.js:833
msgid "Couldn't delete tag!"
msgstr ""

#: taguette/static/js/taguette.js:900
msgid "Couldn't merge tags!"
msgstr ""

#: taguette/static/js/taguette.js:1061
msgid "Couldn't create highlight!"
msgstr ""

#: taguette/static/js/taguette.js:1081
msgid "Couldn't delete highlight!"
msgstr ""

#: taguette/static/js/taguette.js:1111
msgid "Full permissions"
msgstr ""

#: taguette/static/js/taguette.js:1112
msgid "Can't change collaborators / delete project"
msgstr ""

#: taguette/static/js/taguette.js:1113
msgid "View & make changes"
msgstr ""

#: taguette/static/js/taguette.js:1114
msgid "View only"
msgstr ""

#: taguette/static/js/taguette.js:1224
msgid "Couldn't update collaborators!"
msgstr ""

#: taguette/static/js/taguette.js:1251
msgid "Load a document on the left"
msgstr ""

#: taguette/static/js/taguette.js:1295
msgid "Error loading document!"
msgstr ""

#: taguette/static/js/taguette.js:1343
msgid "No highlights with this tag yet."
msgstr ""

#: taguette/static/js/taguette.js:1359
msgid "Error loading tag highlights!"
msgstr ""

#: taguette/static/js/taguette.js:1469
msgid "It appears that you have been logged out."
msgstr ""

#: taguette/static/js/taguette.js:1472
msgid "You can no longer access this project."
msgstr ""

